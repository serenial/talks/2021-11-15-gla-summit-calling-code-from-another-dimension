# Slides

**See this repos .pdf file for slides**

# Bibliography
Here are the links from the talk:

### Our Giants are Female
* [Proffessor Samia Nefti-Meziani Profile](https://www.salford.ac.uk/our-staff/samia-nefti-meziani)

### Scenario One
Configuring the Call Library Function Node
* [Youtube Video: Using Shared Libraries in LabVIEW ](https://www.youtube.com/watch?v=CdG--UMpNhg)
* [NI-Forum Tutorial: Configuring the Call Library Function Node](https://forums.ni.com/t5/Developer-Center-Resources/Tutorial-Configuring-the-Call-Library-Function-Node-to-call-a/ta-p/3522246)

Dereferencing Pointers
* [NI-Forum Tutorial: Dereferencing Pointers Returned by Call Library Function Node](https://forums.ni.com/t5/Developer-Center-Resources/Dereferencing-Pointers-from-C-C-DLLs-in-LabVIEW/ta-p/3522795)

Checking Dependencies with Dependency Walker and Dependencies (modern rewrite)
* [GitHub: lucasg/Dependencies](https://github.com/lucasg/Dependencies)

### Scenario Two
Creating a Wrapper for C++ code
* [LabVIEW Wiki: DLL/Shared Library](https://labviewwiki.org/wiki/DLL/shared_library)

LabVIEW Manager Functions
* [LabVIEW Documentation: LabVIEW Manager Functions](https://zone.ni.com/reference/en-XX/help/371361R-01/lvexcodeconcepts/labview_manager_routines/)

Setting Up Visual Studio Code for building and debugging with LabVIEW
* [GitLab: My North-West User Group (UK) talk from 2020](https://gitlab.com/serenial/talks/11-20-labview-and-vscode-shared-libraries)

C++ Dependency Manager
* [`vcpkg.io`](https://vcpkg.io/en/index.html)

## Scencario Three
Generating LabVIEW User Events from C/C++ code
* [LabVIEW Documentation: `PostLVUserEvent` function call](https://zone.ni.com/reference/en-XX/help/371361R-01/lvexcode/postlvuserevent/)

## Bonus Content
Setting a LabVIEW Occurence from C/C++ code
* [LabVIEW Documentation: `occur` function call](https://zone.ni.com/reference/en-XX/help/371361R-01/lvexcode/occur/)

My open source project which uses lots of the topics discussed in the talk
* [Gitlab: Asynchronous System Exec](https://gitlab.com/serenial/asynchronous-system-exec)

A discussion about returing complex data structures which require multiple allocations
* [LAVA.org: DLL Linked List To Array of Strings](https://lavag.org/topic/21269-dll-linked-list-to-array-of-strings/)

Memory Managment Library (limited documents but looks promising)
* [GitHub: ni/LabVIEW management](https://github.com/ni/labview-memory-management-tools)
